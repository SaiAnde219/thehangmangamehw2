package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 * @author Sai Ande
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              goodLetters;        // displays all good letters guessed
    HBox              badLetters;        // displays all bad letters guessed
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    Button            hint;              //hint button if more than 7 unique letters in target word
    HangmanController controller;
    Pane              figurePane;       // container to display the namesake graphic of the (potentially) hanging person
    Shape             platform;
    Shape             wall;
    Shape             top;
    Shape             hang;
    Shape             head;
    Shape             body;
    Shape             arm1;
    Shape             arm2;
    Shape             leg1;
    Shape             leg2;


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
        hintHandlers();
    }

    public void getPlatform()
    {
        platform.setVisible(true);
        platform.setFill(Paint.valueOf("#000000"));
    }
    public void getWall()
    {
        wall.setVisible(true);

    }
    public void getTop()
    {
        top.setVisible(true);

    }
    public void getHang()
    {
        hang.setVisible(true);

    }
    public void getHead()
    {
        head.prefHeight(20);
        head.prefWidth(20);
        head.setFill(Paint.valueOf("#000000"));
        head.setVisible(true);
    }
    public void getBody()
    {
        body.setVisible(true);

    }
    public void getArm1()
    {
        arm1.setVisible(true);

    }
    public void getArm2()
    {
        arm2.setVisible(true);

    }
    public void getLeg1()
    {
        leg1.setVisible(true);

    }
    public void getLeg2()
    {
        leg2.setVisible(true);

    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new Pane();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        goodLetters = new HBox();
        goodLetters.setStyle("-fx-background-color: transparent;");
        badLetters = new HBox();
        badLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();

       figurePane.setPadding(new Insets(100,100,100,100));


        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, goodLetters, badLetters);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(gameTextsPane);

        startGame = new Button("Start Playing");
        hint = new Button("Hint");
        hint.setVisible(false);
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight, hint);

        platform = new Rectangle(200, 6, Paint.valueOf("#000000"));
        ((Rectangle)platform).setX(300);
        ((Rectangle)platform).setY(400);
        platform.setVisible(false);
        wall = new Rectangle(6, 300, Paint.valueOf("#000000"));
        ((Rectangle)wall).setX(300);
        ((Rectangle)wall).setY(100);
        wall.setVisible(false);
        top = new Rectangle(90, 6, Paint.valueOf("#000000"));
        ((Rectangle)top).setX(300);
        ((Rectangle)top).setY(106);
        top.setVisible(false);
        hang = new Rectangle(6, 30, Paint.valueOf("#000000"));
        ((Rectangle)hang).setX(384);
        ((Rectangle)hang).setY(112);
        hang.setVisible(false);
        head = new Circle(20);
        ((Circle)head).setCenterX(386);
        ((Circle)head).setCenterY(162);
        head.setVisible(false);
        body = new Rectangle(6, 70, Paint.valueOf("000000"));
        ((Rectangle)body).setX(384);
        ((Rectangle)body).setY(182);
        body.setVisible(false);
        arm1 = new Rectangle(4, 30, Paint.valueOf("#000000"));
        arm1.setVisible(false);
        ((Rectangle)arm1).setX(392);
        ((Rectangle)arm1).setY(188);
        arm1.setRotate(-30);
        arm2 = new Rectangle(4, 30, Paint.valueOf("#000000"));
        ((Rectangle)arm2).setX(378);
        ((Rectangle)arm2).setY(188);
        arm2.setRotate(30);
        arm2.setVisible(false);
        leg1 = new Rectangle(4, 30, Paint.valueOf("#000000"));
        ((Rectangle)leg1).setX(392);
        ((Rectangle)leg1).setY(249);
        leg1.setRotate(-30);
        leg1.setVisible(false);
        leg2 = new Rectangle(4, 30, Paint.valueOf("#000000"));
        ((Rectangle)leg2).setX(378);
        ((Rectangle)leg2).setY(249);
        leg2.setRotate(30);
        leg2.setVisible(false);


       figurePane.getChildren().addAll(platform, wall, top, hang, head, body, arm1, arm2, leg1, leg2);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar, figurePane);
    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    private void hintHandlers() {
        hint.setOnMouseClicked(e -> controller.hint());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public Button getHint()
    {
        hint.setVisible(true);
        hint.setDisable(false);
        return hint;
    }

    public void reinitialize() {
        guessedLetters = new HBox(5);
        guessedLetters.setPadding(new Insets(5,5,5,5));
        guessedLetters.setStyle("-fx-background-color: transparent;");
        goodLetters = new HBox();
        goodLetters.setStyle("-fx-background-color: transparent;");
        badLetters = new HBox();
        badLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, goodLetters, badLetters);
        bodyPane.getChildren().setAll(gameTextsPane);
        platform.setVisible(false);
        wall.setVisible(false);
        top.setVisible(false);
        hang.setVisible(false);
        head.setVisible(false);
        body.setVisible(false);
        arm1.setVisible(false);
        arm2.setVisible(false);
        leg1.setVisible(false);
        leg2.setVisible(false);
        hint.setVisible(false);
    }
}
