package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 * @author Sai Ande
 */

//comment
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate;    // shared reference to the application
    private GameData    gamedata;       // shared reference to the game being played, loaded or saved
    private GameState   gamestate;      // the state of the game being shown in the workspace
    private Text[]      progress;       // reference to the text area for the word
    private boolean     success;        // whether or not player was successful
    private int         discovered;     // the number of letters already discovered
    private Button      gameButton;     // shared reference to the "start game" button
    private Label       remains;        // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private HBox        goodLetters;
    private HBox        badLetters;





    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;


        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        goodLetters            = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        badLetters            = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(3);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        goodLetters.getChildren().addAll(new Label("Good Letters: "));
        badLetters.getChildren().addAll(new Label("Bad Letters: "));
        initWordGraphics(guessedLetters);
        System.out.println(gamedata.getTargetWord());
        isHint();

        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success)
                endMessage += String.format("");
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        //isHint();

            for (int i = 0; i < progress.length; i++) {
                progress[i] = new Text(Character.toString(targetword[i]));
                progress[i].setVisible(false);
                StackPane pane = new StackPane();
                pane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#FFFFFF"), CornerRadii.EMPTY, Insets.EMPTY)));
                pane.setPrefSize(30, 30);
                pane.getChildren().add(progress[i]);
                guessedLetters.getChildren().addAll(pane);
            }
    }

private void isHint()
{
    char[] targetword = gamedata.getTargetWord().toCharArray();
    if(targetword.length>7)
    {
        {
            int lettersCount = 0;
            for (int j = 0; j < targetword.length; j++)
            {
                int count = 0;
                for(int k=j; k<targetword.length-1; k++)
                {
                    if (targetword[j]==(targetword[k + 1]))
                        count++;
                }
                if (count==0)
                    lettersCount++;

            }
            if(lettersCount> 7) {
                gamedata.setHintable(true);
                ((Workspace) appTemplate.getWorkspaceComponent()).getHint();

            }
        }
    }

}
    public void hint()
    {
        setGameState(GameState.INITIALIZED_MODIFIED);
        gamedata.setHintable(true);
        char[] targetword = gamedata.getTargetWord().toCharArray();
        char letter;
        while (true) {
            Random random = new Random();
            int index = random.nextInt(targetword.length);
            letter = targetword[index];
            if(!alreadyGuessed(letter))
                break;

        }
        gamedata.remainingGuesses--;
        gamedata.addGoodGuess(letter);
        for (int i = 0; i < progress.length; i++) {
            if (targetword[i]== letter) {
                progress[i].setVisible(true);
                discovered++;
                if(gamedata.getRemainingGuesses()==9)
                    ((Workspace)appTemplate.getWorkspaceComponent()).getPlatform();
                else if(gamedata.getRemainingGuesses()==8)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
                else if(gamedata.getRemainingGuesses()==7)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
                else if(gamedata.getRemainingGuesses()==6)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
                else if(gamedata.getRemainingGuesses()==5)
                    ((Workspace)appTemplate.getWorkspaceComponent()).getHead();
                else if(gamedata.getRemainingGuesses()==4)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
                else if(gamedata.getRemainingGuesses()==3)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getArm1();
                else if(gamedata.getRemainingGuesses()==2)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getArm2();
                else if(gamedata.getRemainingGuesses()==1)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getLeg1();
                else if(gamedata.getRemainingGuesses()==0)
                    ((Workspace) appTemplate.getWorkspaceComponent()).getLeg2();

                remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                ((Workspace)appTemplate.getWorkspaceComponent()).getHint().setDisable(true);
                success = (discovered == progress.length);
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    end();
            }
        }
        Text text = new Text((Character.toString(letter)));

        goodLetters.getChildren().add(text);
        goodLetters.getChildren().add(new Label(", "));
        gamedata.setHintable(false);

    }



    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if(guess<'a'||guess>'z') {
                        PropertyManager           manager    = PropertyManager.getManager();
                        AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
                        //System.out.println("Error");
                        dialog.show(manager.getPropertyValue(TRY_AGAIN), "Please press something in A-Z.");
                    }
                    else {
                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            Text text = new Text((Character.toString(guess)));
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                }
                            }
                            goodLetters.getChildren().add(text);
                            if(goodguess==true)
                            goodLetters.getChildren().add(new Label(", "));
                            if (!goodguess) {
                                gamedata.addBadGuess(guess);
                                badLetters.getChildren().add(text);
                                badLetters.getChildren().add(new Label(", "));
                                if(gamedata.getRemainingGuesses()==9)
                                    ((Workspace)appTemplate.getWorkspaceComponent()).getPlatform();
                                else if(gamedata.getRemainingGuesses()==8)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
                                else if(gamedata.getRemainingGuesses()==7)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
                                else if(gamedata.getRemainingGuesses()==6)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
                                else if(gamedata.getRemainingGuesses()==5)
                                    ((Workspace)appTemplate.getWorkspaceComponent()).getHead();
                                else if(gamedata.getRemainingGuesses()==4)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
                                else if(gamedata.getRemainingGuesses()==3)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getArm1();
                                else if(gamedata.getRemainingGuesses()==2)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getArm2();
                                else if(gamedata.getRemainingGuesses()==1)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getLeg1();
                                else if(gamedata.getRemainingGuesses()==0)
                                    ((Workspace) appTemplate.getWorkspaceComponent()).getLeg2();
                            }
                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        }
                    }
                    setGameState(GameState.INITIALIZED_MODIFIED);
                });
                if (gamedata.getRemainingGuesses() <= 0 || success) {
                    for (int i = 0; i < progress.length; i++) {

                        if(!progress[i].isVisible()) {
                            progress[i].setStrokeWidth(0.5);
                            progress[i].setStroke(Paint.valueOf("#FF0000"));
                            progress[i].setVisible(true);
                        }
                    }
                    stop();
                }
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        goodLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(2);
        badLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(3);
        goodLetters.getChildren().addAll(new Label("Good Letters: "));
        badLetters.getChildren().addAll(new Label("Bad Letters: "));
        restoreWordGraphics(guessedLetters);
        restoreGoodLettersGraphics(goodLetters);
        restoreBadLettersGraphics(badLetters);
        restoreHangmanGraphics();
        restoreIsHint();

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);


        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;

            StackPane pane = new StackPane();
            pane.setBackground(new Background(new BackgroundFill(Paint.valueOf("#FFFFFF"), CornerRadii.EMPTY, Insets.EMPTY)));
            pane.setPrefSize(30, 30);
            pane.getChildren().add(progress[i]);
            guessedLetters.getChildren().addAll(pane);
        }
    }

    private void restoreGoodLettersGraphics(HBox goodLetters) {
        char[] good = gamedata.getGoodGuesses().toString().toCharArray();
        for (int i = 1; i < good.length-1; i++) {
            Text text = new Text(Character.toString(good[i]));
            goodLetters.getChildren().add(text);
        }
        goodLetters.getChildren().addAll(new Label(", "));
    }
    private void restoreBadLettersGraphics(HBox badLetters) {
        char[] bad = gamedata.getBadGuesses().toString().toCharArray();
        for(int j=1; j<bad.length-1; j++) {
            Text text = new Text(Character.toString(bad[j]));
            badLetters.getChildren().add(text);
        }
        badLetters.getChildren().addAll(new Label(", "));
    }


    public void restoreHangmanGraphics()
    {
        if(gamedata.getRemainingGuesses()==9)
            ((Workspace)appTemplate.getWorkspaceComponent()).getPlatform();
        else if(gamedata.getRemainingGuesses()==8) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
        }
        else if(gamedata.getRemainingGuesses()==7) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
        }
        else if(gamedata.getRemainingGuesses()==6) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
        }
        else if(gamedata.getRemainingGuesses()==5)
        {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
            ((Workspace)appTemplate.getWorkspaceComponent()).getHead();
        }

        else if(gamedata.getRemainingGuesses()==4) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHead();
            ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
        }
        else if(gamedata.getRemainingGuesses()==3) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHead();
            ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm1();
        }
        else if(gamedata.getRemainingGuesses()==2) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHead();
            ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm1();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm2();
        }
        else if(gamedata.getRemainingGuesses()==1) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHead();
            ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm1();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm2();
            ((Workspace) appTemplate.getWorkspaceComponent()).getLeg1();
        }
        else if(gamedata.getRemainingGuesses()==0) {
            ((Workspace) appTemplate.getWorkspaceComponent()).getPlatform();
            ((Workspace) appTemplate.getWorkspaceComponent()).getWall();
            ((Workspace) appTemplate.getWorkspaceComponent()).getTop();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHang();
            ((Workspace) appTemplate.getWorkspaceComponent()).getHead();
            ((Workspace) appTemplate.getWorkspaceComponent()).getBody();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm1();
            ((Workspace) appTemplate.getWorkspaceComponent()).getArm2();
            ((Workspace) appTemplate.getWorkspaceComponent()).getLeg1();
            ((Workspace) appTemplate.getWorkspaceComponent()).getLeg2();
        }
    }

    public void restoreIsHint()
    {
        if(gamedata.isHintable())
            ((Workspace) appTemplate.getWorkspaceComponent()).getHint();
        else
            ((Workspace) appTemplate.getWorkspaceComponent()).getHint().setDisable(true);
    }


    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;

        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                    String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists())
                load(selectedFile.toPath());
            restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */

    private void save(Path target) throws IOException {

        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        GameData gamedatatest = (GameData) appTemplate.getDataComponent();

        System.out.println(gamedatatest.hintable+" --"+gamedatatest.getTargetWord());
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));

    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
